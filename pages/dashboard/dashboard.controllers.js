(function (angular) {

    var module = angular.module('dashboardModule', []);

    module.controller("dashboardController", function ($scope, $controller, $timeout, Dashboard, DataAccess, ngToast) {

        $scope.loading = true;
        DataAccess.read("classes").then(function (response) {
            $scope.classesresults = response;
        });

        DataAccess.read("streams").then(function (response) {
            $scope.streamsresults = response;
        });

        DataAccess.read("students").then(function (response) {
            $scope.studentsresults = response;
        });

        DataAccess.read("users").then(function (response) {
            $scope.usersresults = response;
        });

        $timeout(function () {
            $scope.totalClasses = $scope.classesresults.length;
            $scope.totalStreams = $scope.streamsresults.length;
            $scope.totalStudents = $scope.studentsresults.length;
            $scope.totalUsers = $scope.usersresults.length;
            $scope.loading = false;
        }, 1000);

        $scope.term = "1";
        $scope.year = (new Date()).getFullYear().toString();

        $scope.setYear = function (y) {
            if ($scope.term) {
                $scope.checkAmount();
            } else {
                alert('Please select term first');
            }
        }

        $scope.setTerm = function (t) {
            if ($scope.year) {
                $scope.checkAmount();
            }
        }

        $scope.checkAmount = function () {
            $scope.action = 'Checking ...';
            $scope.loading = true;
            DataAccess.checkAmount("fees", $scope.term, $scope.year).then(function (response) {
                $scope.feeResult = response;
            });

            DataAccess.checkPayments("payments", $scope.term, $scope.year, null).then(function (response) {
                $scope.paymentsResult = response;
            });

            $timeout(function () {
                $scope.fees = $scope.feeResult;
                $scope.results = $scope.paymentsResult;
                $scope.amountPaid = 0;
                $scope.balance = 0;
                angular.forEach($scope.results, function (u, i) {
                    $scope.amountPaid += $scope.results[i].amountPaid;
                });

                $scope.amountRequired = 0;
                if ($scope.fees.length > 0) {
                    angular.forEach($scope.fees, function (u, i) {
                        var details = {};
                        details.class = $scope.fees[i].classId;
                        var count = 0;
                        $scope.amountRequired = 0;

                        DataAccess.pages("students", details).then(function (response) {
                            $scope.studentsresults = response;
                            count = $scope.studentsresults.length;
                        });
                        $timeout(function () {
                            var total = $scope.fees[i].amountToPay * count;
                            $scope.amountRequired += total;
                        }, 1000);
                    });
                }
                $scope.loading = false;
            }, 1000);
        };
        $scope.checkAmount();

    });


})(angular);