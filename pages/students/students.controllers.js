(function (angular) {

    var module = angular.module('studentsModule', []);

    module.controller("listStudentsController", function ($scope, $timeout, DataAccess, ngToast) {
        $scope.classes = [];
        $scope.streams = [];
        $scope.results = [];
        $scope.narrative = '';
        $scope.profession = [];
        $scope.display = true;

        $scope.itemsPerPage = 10;

        $scope.invert = function () {
            $scope.display = !$scope.display;
        }

        $scope.initiliseFilters = function () {
            $scope.filters = {
                class: null,
                stream: null,
            };
        }

        $scope.initiliseFilters();

        $scope.getDetails = function () {
            $scope.totalItems = $scope.students.length;
            $scope.loading = false
        };

        DataAccess.read("classes").then(function (response) {
            $scope.classesresults = response;
        });

        DataAccess.read("streams").then(function (response) {
            $scope.streamsresults = response;
        });

        $timeout(function () {
            $scope.classes = $scope.classesresults;
            $scope.streams = $scope.streamsresults;
            var start = { id: "*", name: "All" };
            $scope.classes.unshift(start);
            $scope.streams.unshift(start);
        }, 1000);


        $scope.filter = function (filters) {
            $scope.loading = true;
            $scope.students = [];

            var stream = JSON.parse($scope.filters.stream);
            var classs = JSON.parse($scope.filters.class);
            var details = {
                class: null,
                stream: null
            };

            if (classs != null && classs.id != "*") {
                details.class = classs.id;
            }
            if (stream != null && stream.id != "*") {
                details.stream = stream.id;
            }

            DataAccess.pages("students", details).then(function (result) {
                $scope.studentresults = result;
            });

            $timeout(function () {
                $scope.totalItems = $scope.studentresults.length;
                $scope.students = $scope.studentresults;
                $scope.loading = false;
            }, 1000);

        }

        if ($scope.reportsText) {
            $scope.narrative = 'reports Results for: ' + $scope.reportsText;
            $scope.performreports();
        }

    });

    module.controller('uploadStudentController', function ($scope, $state, $parse, $localStorage, $timeout, DataAccess, ngToast) {
        $scope.Math = window.Math;
        $scope.studentCount = 0;
        $scope.success = 0;
        $scope.failed = 0;
        $scope.step = 1;
        $scope.done = false;
        $scope.display = false;

        $scope.filters = {
            class: null,
            stream: null,
        };

        DataAccess.read("classes").then(function (response) {
            $scope.classesresults = response;
        });

        DataAccess.read("streams").then(function (response) {
            $scope.streamsresults = response;
        });

        $timeout(function () {
            $scope.classes = $scope.classesresults;
            $scope.streams = $scope.streamsresults;
        }, 1000);

        if ($localStorage.students) {
            $scope.step = 2;
            $scope.students = $localStorage.students;
            validateStudents($scope, angular);
        }
        function validateStudents($scope, angular) {
            $scope.countinvalid = 0;
            angular.forEach($scope.students, function (u, i) {
                var valid = true;
                if (
                    $scope.students[i].surname === undefined ||
                    $scope.students[i].otherNames === undefined ||
                    $scope.students[i].phone1 === undefined ||
                    $scope.students[i].phone2 === undefined
                ) {
                    $scope.countinvalid = $scope.countinvalid + 1;
                    valid = false;
                }
                $scope.students[i].valid = valid;
            });
        }




        $scope.nextStep = function () {
            $scope.step++;
            if ($scope.step == 2) {
                $localStorage.students = $scope.csv.result;
                $scope.students = $localStorage.students;
                validateStudents($scope, angular);
            }
        }

        $scope.clearStudents = function () {
            $scope.step = 1;
            $localStorage.students = null;
            $scope.initialiseUpload();
            $scope.students = $localStorage.students;
        }



        $scope.Math = window.Math;
        $scope.initialiseUpload = function () {
            $scope.csv = {
                content: null,
                header: true,
                headerVisible: true,
                separator: ',',
                separatorVisible: false,
                result: null,
                encoding: 'ISO-8859-1',
                encodingVisible: false,
                uploadButtonLabel: "Upload Students",
                progressCallback: function (progress) {
                    $scope.$apply(function () {
                        $scope.progress = progress;
                    });
                },
                streamingCallback: function (stream) {
                    if (typeof stream != "undefined") {
                        $scope.$apply(function () {
                            $scope.preview = stream[Math.floor(Math.random() * stream.length)];
                        });
                    }
                },
                streamingErrorCallback: function (streamError) {
                    console.log(streamError);
                }
            };
        }
        $scope.initialiseUpload();


        var _lastGoodResult = '';
        $scope.toPrettyJSON = function (json, tabWidth) {
            var objStr = JSON.stringify(json);
            var obj = null;
            try {
                obj = $parse(objStr)({});
            } catch (e) {
                return _lastGoodResult;
            }
            var result = JSON.stringify(obj, null, Number(tabWidth));
            _lastGoodResult = result;
            return result;
        };


        $scope.$watch('stream', function (newVal, oldVal) {
            if (newVal != oldVal) {
                $scope.filters.stream = newVal;
            }
        });
        $scope.$watch('class', function (newVal, oldVal) {
            if (newVal != oldVal) {
                $scope.filters.class = newVal;
            }
        });

        $scope.completeUpload = function (students) {
            $scope.display = true;
            $scope.submitting = true;
            $scope.studentCount = students.length;
            var stream = JSON.parse($scope.stream);
            var classs = JSON.parse($scope.class);
            angular.forEach(students, function (u, i) {
                var element = students[i];
                var student = {
                    id: element.studentId,
                    surname: element.surname,
                    studentId: element.studentId,
                    otherNames: element.otherNames,
                    phone1: element.phone1,
                    phone2: element.phone2,
                    classId: classs.id,
                    className: classs.name,
                    streamId: stream.id,
                    streamName: stream.name
                }
                DataAccess.create("students", student).then(function (result) {
                    if (result) {
                        $scope.success += 1;
                        $scope.successPercent = ($scope.success / $scope.studentCount) * 100;
                    } else {
                        $scope.failed += 1;
                        $scope.failedPercent = ($scope.failed / $scope.studentCount) * 100;
                    }
                });
            });
            $scope.submitting = false;
            $scope.done = true;
        };
    });

    module.controller('addStudentController', function ($scope, $state, $timeout, DataAccess, ngToast) {
        $scope.title = 'Add student';
        $scope.action = 'add';

        DataAccess.read("classes").then(function (response) {
            $scope.classesresults = response;
        });

        DataAccess.read("streams").then(function (response) {
            $scope.streamsresults = response;
        });

        $timeout(function () {
            $scope.classes = $scope.classesresults;
            $scope.streams = $scope.streamsresults;
        }, 1000);

        $scope.save = function (student) {
            student.class = JSON.parse(student.class);
            student.stream = JSON.parse(student.stream);
            var details = {
                id: student.studentid,
                studentid: student.studentid,
                surname: student.surname,
                otherNames: student.otherNames,
                phone1: student.phone1,
                phone2: student.phone2,
                classId: student.class.id,
                className: student.class.name,
                streamId: student.stream.id,
                streamName: student.stream.name,
            };

            DataAccess.create("students", details).then(function (result) {
                if (result) {
                    ngToast.create({ className: 'success', content: '<i class="fa fa-check-circle"></i> Saved successful' });
                    $state.go('students');
                } else {
                    ngToast.create({ className: 'danger', content: '<i class="fa fa-warning"></i> Failed!' });
                }
            });
        }
    });

    module.controller('editStudentController', function ($scope, $state, $stateParams, $timeout, DataAccess, ngToast) {
        $scope.loading = true;
        $scope.action = 'edit';
        
        DataAccess.read("classes").then(function (response) {
            $scope.classesresults = response;
        });

        DataAccess.read("streams").then(function (response) {
            $scope.streamsresults = response;
        });

        var id = $stateParams.id;
        DataAccess.get("students", id).then(function (response) {
            $scope.results = response[0];
        });

        $timeout(function () {
            $scope.classes = $scope.classesresults;
            $scope.streams = $scope.streamsresults;
            $scope.student = $scope.results;
            $scope.student.phone1 = parseInt($scope.student.phone1);
            $scope.student.phone2 = parseInt($scope.student.phone2);
            $scope.title = 'Edit student: ' + $scope.student.otherNames;
            $scope.loading = false;
        }, 1000);

        $scope.save = function (student) {
            var details = {
                studentid: student.studentid,
                surname: student.surname,
                otherNames: student.otherNames,
                phone1: student.phone1,
                phone2: student.phone2,
                classId: student.class.id,
                className: student.class.name,
                streamId: student.stream.id,
                streamName: student.stream.name,
            };
            DataAccess.update("students", id, details).then(function (result) {
                if (result) {
                    ngToast.create({ className: 'success', content: '<i class="fa fa-check-circle"></i> Saved successful' });
                    $state.go('students');
                } else {
                    ngToast.create({ className: 'danger', content: '<i class="fa fa-warning"></i> Failed' });
                }
            });
        }
    });
    module.controller('studentDetailController', function ($scope, $state, $stateParams, $timeout, DataAccess, ngToast) {
        $scope.loading = true;
        $scope.action = 'edit';

        var id = $stateParams.id;
        DataAccess.get("students", id).then(function (response) {
            $scope.results = response[0];
        });
        DataAccess.checkPayments("payments", null, null, id).then(function (response) {
            $scope.historyResults = response;
        });

        $timeout(function () {
            $scope.loading = false;
            $scope.student = $scope.results;
            $scope.history = $scope.historyResults;
            $scope.title = 'Edit student: ' + $scope.student.otherNames;
        }, 1000);

    });




})(angular);