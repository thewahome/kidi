(function (angular) {
    var module = angular.module('dataModule', []);
    module.controller('listStreamsController', function ($scope, $timeout, DataAccess, ngToast) {
        $scope.loading = true;
        $scope.totalItems = 0;
        $scope.itemsPerPage = 5;

        $scope.getDetails = function () {
            $scope.streams = $scope.results;
            $scope.totalItems = $scope.streams.length;
            $scope.loading = false
        };

        DataAccess.read("streams").then(function (response) {
            $scope.results = response;
        });

        $timeout(function () {
            $scope.getDetails();
        }, 1000);
    });

    module.controller('addStreamController', function ($scope, $state, $timeout, DataAccess, ngToast) {

        $scope.title = 'Add stream';
        $scope.stream = {};

        $scope.setUserType = function (userType) {
            $scope.userType = userType;
        }

        $scope.action = 'add';

        $scope.save = function (stream) {

            var details = {
                name: stream.name,
            };

            DataAccess.create("streams", details).then(function (result) {
                if (result) {
                    ngToast.create({ className: 'success', content: '<i stream="fa fa-check-circle"></i> Saved successful' });
                    $state.go('streams');
                } else {
                    ngToast.create({ className: 'danger', content: '<i stream="fa fa-warning"></i> Failed!' });
                }
            });
        }
    });

    module.controller('editStreamController', function ($scope, $state, $stateParams, $timeout, DataAccess, ngToast) {
        $scope.loading = true;
        $scope.action = 'edit';
        var id = $stateParams.id;
        DataAccess.get("streams", id).then(function (response) {
            $scope.results = response[0];
        });

        $scope.getDetails = function () {
            $scope.loading = false;
            $scope.stream = $scope.results;
            console.log($scope.stream)
            $scope.title = 'Edit stream: ' + $scope.stream.name;
        };

        $timeout(function () {
            $scope.getDetails();
        }, 1000);

        $scope.save = function (stream) {
            var details = {
                name: stream.name,
                entityStatus: stream.entityStatus,
            };

            DataAccess.update("streams", $stateParams.id, details).then(function (result) {
                if (result) {
                    ngToast.create({ className: 'success', content: '<i stream="fa fa-check-circle"></i> Saved successful' });
                    $state.go('streams');
                } else {
                    ngToast.create({ className: 'danger', content: '<i stream="fa fa-warning"></i> Failed' });
                }
            });
        }
    });


})(angular);