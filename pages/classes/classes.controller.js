(function (angular) {
    var module = angular.module('classesModule', []);
    module.controller('listClassesController', function ($scope, $timeout, DataAccess, ngToast) {
        $scope.loading = true;
        $scope.totalItems = 0;
        $scope.itemsPerPage = 5;

        $scope.getDetails = function () {
            $scope.classes = $scope.results;
            $scope.totalItems = $scope.classes.length;
            $scope.loading = false
        };

        DataAccess.read("classes").then(function (response) {
            $scope.results = response;
        });

        $timeout(function () {
            $scope.getDetails();
        }, 1000);
    });

    module.controller('addClassController', function ($scope, $state, $timeout, DataAccess, ngToast) {

        $scope.title = 'Add class';
        $scope.class = {};

        $scope.setUserType = function (userType) {
            $scope.userType = userType;
        }

        $scope.action = 'add';

        $scope.save = function (classs) {

            var details = {
                name: classs.name,
                year: 2018,
            };

            DataAccess.create("classes", details).then(function (result) {
                if (result) {
                    ngToast.create({ className: 'success', content: '<i classs="fa fa-check-circle"></i> Saved successful' });
                    $state.go('classes');
                } else {
                    ngToast.create({ className: 'danger', content: '<i classs="fa fa-warning"></i> Failed!' });
                }
            });
        }
    });

    module.controller('editClassController', function ($scope, $state, $stateParams, $timeout, DataAccess, ngToast) {
        $scope.loading = true;
        $scope.action = 'edit';
        var id = $stateParams.id;
        DataAccess.get("classes", id).then(function (response) {
            $scope.results = response[0];
        });

        $scope.getDetails = function () {
            $scope.loading = false;
            $scope.class = $scope.results;
            console.log($scope.class)
            $scope.title = 'Edit classs: ' + $scope.class.name;
        };

        $timeout(function () {
            $scope.getDetails();
        }, 1000);

        $scope.save = function (classs) {
            var details = {
                name: classs.name,
                year: 2018,
                entityStatus: classs.entityStatus,
            };

            DataAccess.update("classes", $stateParams.id, details).then(function (result) {
                if (result) {
                    ngToast.create({ className: 'success', content: '<i classs="fa fa-check-circle"></i> Saved successful' });
                    $state.go('classes');
                } else {
                    ngToast.create({ className: 'danger', content: '<i classs="fa fa-warning"></i> Failed' });
                }
            });
        }
    });


})(angular);