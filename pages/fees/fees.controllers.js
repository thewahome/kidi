(function (angular) {

    var module = angular.module('feesModule', []);

    module.controller("setupFeesController", function ($scope, $timeout, DataAccess, ngToast) {

        $scope.loading = false;
        $scope.action = 'add';
        $scope.term = null;
        $scope.year = null;
        $scope.classes = null;
        $scope.narrative = '';
        $scope.action = '';



        $scope.setYear = function (y) {
            if ($scope.term) {
                $scope.checkAmount();
            } else {
                alert('Please select term first');
                $scope.term = 1;
                $scope.year = null;
            }
        }

        $scope.setTerm = function (t) {
            if ($scope.year) {
                $scope.checkAmount();
            }
        }

        $scope.checkAmount = function () {
            $scope.action = 'Checking ...';
            $scope.loading = true;
            DataAccess.checkAmount("fees", $scope.term, $scope.year).then(function (response) {
                $scope.feeResult = response;
            });

            $timeout(function () {
                $scope.fees = $scope.feeResult;
                $scope.setups = [];
                if ($scope.fees.length == 0) {
                    $scope.fetchClasses();
                    $scope.narrative =
                        'Fees have not been set for Term ' + $scope.term + ' ' + $scope.year + '. Fill in the amounts, click confirm then save once all are done';
                    $scope.action = 'add';
                } else {
                    $scope.action = 'edit';
                    $scope.narrative =
                        'Fees for Term ' + $scope.term + ' ' + $scope.year + ' are already set. To change them, click the row, fill in the amounts, click confirm then save once all are done';

                    angular.forEach($scope.fees, function (u, i) {
                        var fee = {
                            id: $scope.fees[i].id,
                            amountToPay: $scope.fees[i].amountToPay,
                            saved: true,
                            editable: false,
                            className: $scope.fees[i].className,
                            classId: $scope.fees[i].classId,
                        };
                        $scope.setups.push(fee);
                    });
                }
                $scope.loading = false;
            }, 1000);
        };

        $scope.fetchClasses = function () {
            $scope.classes = null;
            $scope.action = 'Checking ...';
            DataAccess.read("classes").then(function (response) {
                $scope.classesresult = response;
            });

            $timeout(function () {
                $scope.classes = $scope.classesresult;
                $scope.setups = [];
                angular.forEach($scope.classes, function (u, i) {
                    var fee = {
                        id: null,
                        amountToPay: null,
                        saved: false,
                        editable: false,
                        className: $scope.classes[i].name,
                        classId: $scope.classes[i].id,
                    };
                    $scope.setups.push(fee);
                });
            }, 1000);
        }

        $scope.save = function () {
            angular.forEach($scope.setups, function (u, i) {
                if ($scope.setups[i].saved) {
                    var fee = {
                        id: $scope.setups[i].id,
                        classId: $scope.setups[i].classId,
                        className: $scope.setups[i].className,
                        amountToPay: $scope.setups[i].amountToPay,
                        term: $scope.term,
                        year: $scope.year,
                    }
                    if ($scope.action == 'edit') {
                        DataAccess.update("fees", $scope.setups[i].id, fee).then(function (result) {
                            if (result) {
                                $scope.success += 1;
                            } else {
                                $scope.failed += 1;
                            }
                        });
                    } else {
                        DataAccess.create("fees", fee).then(function (result) {
                            if (result) {
                                $scope.success += 1;
                            } else {
                                $scope.failed += 1;
                            }
                        });
                    }

                }
            });
            ngToast.create({ className: 'success', content: '<i class="fa fa-check-circle"></i> Saved successful' });
        }
        $scope.update = function () {
            angular.forEach($scope.setups, function (u, i) {
                if ($scope.setups[i].saved) {
                    var fee = {
                        classId: $scope.setups[i].classId,
                        className: $scope.setups[i].className,
                        amountToPay: $scope.setups[i].amountToPay,
                        term: $scope.term,
                        year: $scope.year,
                    }
                    DataAccess.create("fees", fee).then(function (result) {
                        if (result) {
                            $scope.success += 1;
                        } else {
                            $scope.failed += 1;
                        }
                    });
                }
            });
            ngToast.create({ className: 'success', content: '<i class="fa fa-check-circle"></i> Saved successful' });
        }

    });

    module.controller('payFeesController', function ($scope, $state, $timeout, DataAccess, ngToast) {
        $searchActive = false;
        $scope.students = null;
        $scope.payment = {};

        $scope.setTerm = function (t) {
        }
        $scope.setYear = function (y) {
            if ($scope.student) {
                $scope.checkAmount();
            } else {
                alert('Please select student first');
                $scope.payment.term = null;
                $scope.payment.year = null;
            }
        }

        $scope.search = function (term) {
            DataAccess.search("students", term).then(function (response) {
                $scope.searchResults = response;
            });
            $timeout(function () {
                $scope.students = $scope.searchResults;
            }, 1000);
        }

        $scope.selectStudent = function (selected) {
            $scope.searchActive = false;
            $scope.searchText = '';
            $scope.students = null;
            $scope.student = selected;
        }


        $scope.checkAmount = function () {
            $scope.loading = true;
            DataAccess.checkAmount("fees", $scope.payment.term, $scope.payment.year, $scope.student.classId).then(function (response) {
                $scope.amountResult = response;
            });
            DataAccess.checkPayments("payments", $scope.payment.term, $scope.payment.year, $scope.student.id).then(function (response) {
                $scope.paymentsResult = response;
            });

            $timeout(function () {
                $scope.payments = $scope.paymentsResult;
                $scope.totalAmountPaid = 0
                angular.forEach($scope.payments, function (u, i) {
                    $scope.totalAmountPaid += $scope.payments[i].amountPaid;
                });
                $scope.fee = $scope.amountResult[0];
                $scope.fee.amountToPay = ($scope.fee.amountToPay == null) ? 0 : $scope.fee.amountToPay;
                $scope.fee.amountToPay = $scope.fee.amountToPay - $scope.totalAmountPaid;
                $scope.loading = false;
            }, 1000);
        };

        $scope.$watch('payment.amountPaid', function (newVal, oldVal) {
            if (newVal != oldVal) {
                $scope.payment.balance = $scope.fee.amountToPay - $scope.payment.amountPaid;
            }
        });


        $scope.calculateBalance = function () {
            var balance = $scope.fee.amountToPay - $scope.payment.amountPaid;
            $scope.payment.balance = balance;
        };


        $scope.save = function () {
            var payment = {
                studentId: $scope.student.id,
                studentName: $scope.student.otherNames + ' ' + $scope.student.surname,
                classId: $scope.student.classId,
                className: $scope.student.className,
                streamId: $scope.student.streamId,
                streamName: $scope.student.streamName,
                amountPaid: $scope.payment.amountPaid,
                reference: $scope.payment.reference,
                balance: $scope.payment.balance,
                term: $scope.payment.term,
                year: $scope.payment.year,
                paidBy: $scope.payment.paidBy,
                balanceDeadline: ($scope.payment.balance == 0) ? 'null' : $scope.payment.balanceDeadline,
            }

            DataAccess.create("payments", payment).then(function (result) {
                if (result) {
                    ngToast.create({ className: 'success', content: '<i class="fa fa-check-circle"></i> Saved successful' });
                    $searchActive = false;
                    $scope.students = null;
                    $scope.student = null;
                    $scope.fee = null;
                    $scope.payment = {};
                    $state.go("fee-receipt", { "id": result });
                } else {
                    ngToast.create({ className: 'danger', content: '<i class="fa fa-warning"></i> Failed' });
                }
            });
            ngToast.create({ className: 'success', content: '<i class="fa fa-check-circle"></i> Saved successful' });

        }


    });

    module.controller('feeReceiptController', function ($scope, $state, $stateParams, $timeout, $rootScope, DataAccess, ngToast) {
        $scope.loading = true;
        debugger
        $rootScope.displayMenu = false;
        var id = $stateParams.id;
        DataAccess.get("payments", id).then(function (response) {
            $scope.result = response[0];
        });

        $timeout(function () {
            $scope.payment = $scope.result;
            $scope.loading = false;
        }, 1000);
    });
    module.controller('feeReportsController', function ($scope, $state, $stateParams, $localStorage, $timeout, DataAccess, ngToast) {

        $scope.initiliseFilters = function () {
            $scope.filters = {
                class: null,
                stream: null,
                term: null,
                year: null,
                paid: true
            };
            $scope.payments = null;
            $scope.students = null;
            $scope.list = null;

        }

        DataAccess.read("classes").then(function (response) {
            $scope.classesresult = response;
        });
        DataAccess.read("streams").then(function (response) {
            $scope.streamsresult = response;
        });

        $timeout(function () {
            $scope.classes = $scope.classesresult;
            $scope.streams = $scope.streamsresult;
            var start = { id: "*", name: "All" };
            $scope.classes.unshift(start);
            $scope.streams.unshift(start);
        }, 1000);

        $scope.initiliseFilters();


        $scope.$watch('filters.paid', function (newVal, oldVal) {
            if (newVal != oldVal) {
                $scope.payments = null;
                $scope.students = null;
                $scope.list = null;

                if (!newVal && ($scope.filters.term == null || $scope.filters.year == null)) {
                    alert("You must set the term and year");
                    $scope.filters.paid = true;
                }
            }
        });

        $scope.payments = $localStorage.payments;

        function groupPayments(payments) {
            var sumBalance = function (array) {
                var total = 0;
                for (var i = 0; i < array.length; i++) {
                    total += array[i].balance;
                }
                return total;
            };
            var sumTotal = function (array) {
                var total = 0;
                for (var i = 0; i < array.length; i++) {
                    total += array[i].amountPaid;
                }
                return total;
            };
            var paymentArrayHolder = [];
            var paymentArray = [];
            payments.forEach(function (item) {
                paymentArrayHolder[item.studentName] = paymentArrayHolder[item.studentName] || {};
                var obj = paymentArrayHolder[item.studentName];
                if (Object.keys(obj).length == 0)
                    paymentArray.push(obj);
                obj.studentId = item.studentId;
                obj.studentName = item.studentName;
                obj.className = item.className;
                obj.streamName = item.streamName;
                obj.details = obj.details || [];
                obj.details.push({
                    date: item.dateCreated,
                    balance: item.balance,
                    amountPaid: item.amountPaid,
                    paidBy: item.paidBy,
                    reference: item.reference,
                    term: item.term,
                    year: item.year,
                });
                obj.countPayments = obj.details.length;
                obj.balance = sumBalance(obj.details);
                obj.amountPaid = sumTotal(obj.details);
            });
            return paymentArray;
        }

        function searchPaidStudents(details) {
            DataAccess.pages("payments", details).then(function (result) {
                $scope.paymentresults = result;
            });
            $timeout(function () {
                $scope.payments = groupPayments($scope.paymentresults);
                $scope.totalItems = $scope.payments.length;
                $scope.loading = false;
            }, 1000);
        }

        function searchUnpaidStudents(params, details) {

            DataAccess.pages("students", params).then(function (response) {
                $scope.searchResults = response;
            });
            $timeout(function () {
                $scope.students = $scope.searchResults;
                $scope.list = [];
                angular.forEach($scope.students, function (u, i) {
                    var student = $scope.students[i];
                    DataAccess.checkAmount("fees", details.term, details.year, student.classId).then(function (response) {
                        $scope.amountResult = response;
                    });
                    DataAccess.checkPayments("payments", details.term.toString(), details.year.toString(), student.id).then(function (response) {
                        $scope.paymentsResult = response;
                        var data = {};
                        $scope.payments = $scope.paymentsResult;
                        $scope.totalAmountPaid = 0;
                        angular.forEach($scope.payments, function (u, i) {
                            $scope.totalAmountPaid += $scope.payments[i].amountPaid;
                        });
                        data.paid = $scope.totalAmountPaid;
                        $scope.fee = $scope.amountResult[0];
                        $scope.fee.amountToPay = ($scope.fee.amountToPay == null) ? 0 : $scope.fee.amountToPay;
                        data.balance = $scope.fee.amountToPay - $scope.totalAmountPaid;
                        data.fee = $scope.fee;
                        data.payments = $scope.payments;
                        data.student = student;
                        if (data.balance > 0)
                            $scope.list.push(data);
                        $scope.loading = false;
                    });
                });
                $scope.totalItems = $scope.list.length;
            }, 2000);
        }

        $scope.filter = function (filters) {

            $scope.loading = true;
            $scope.students = [];
            var start = { id: "*", name: "All" };
            var stream = (filters.stream == null) ? start : JSON.parse(filters.stream);
            var classs = (filters.class == null) ? start : JSON.parse(filters.class);
            var term = JSON.parse(filters.term);
            var year = JSON.parse(filters.year);

            var details = {
                class: null,
                stream: null,
                term: null,
                year: null,
            };

            if (classs != null && classs.id != "*") {
                details.class = classs.id;
            }
            if (stream != null && stream.id != "*") {
                details.stream = stream.id;
            }
            details.term = term;
            details.year = year;

            if (filters.paid) {
                searchPaidStudents(details);
            } else {
                var params = {
                    class: details.class,
                    stream: details.stream,
                }

                searchUnpaidStudents(params, details);

            }
        }
    });
})(angular);







