(function (angular) {

    var module = angular.module('loginModule', []);

    module.controller("loginController", function ($scope, $state, $rootScope, DataAccess, $location, $localStorage) {

        $scope.errorMessage = '';

        this.loginUser = function () {
            var details = { username: $scope.username, password: $scope.password };

            DataAccess.findUser(details).then(function (result) {
                if (result.length > 0)
                    $scope.processResult(result);
                else
                    $scope.errorMessage = 'Login Failed. Please try again';
            });
        }

        $scope.processResult = function (result) {
            if (result) {
                var user = result[0];
                $localStorage.user = user;
                window.location.href = '#/dashboard';
            } else {
                $scope.errorMessage = result.message;
            }
        }





        this.resendPassword = function () {
            var details = { username: this.username, mobileNumber: this.mobileNumber };

            DataAccess.resendPassword({}, details)
                .$promise.then(function (result) {
                    if (result.Successful) {
                        $scope.message = result.Message;
                    }
                    $scope.errorMessage = result.ErrorMessage;
                });
        };

    });
})(angular);
