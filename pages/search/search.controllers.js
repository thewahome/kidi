(function (angular) {

    var module = angular.module('searchModule', []);

    module.controller("searchController", function ($scope, $stateParams,$timeout, DataAccess, ngToast) {
        $scope.display = 0;
        $scope.searchText = $stateParams.searchText;
        $scope.narrative = 'To search, enter anything in the search box';
        $scope.performSearch = function () {
            DataAccess.search("students", $scope.searchText).then(function (response) {
                $scope.searchResults = response;
            });
            $timeout(function () {
                $scope.students = $scope.searchResults;
            }, 1000);
        }

        if ($scope.searchText) {
            $scope.narrative = 'Search Results for: ' + $scope.searchText;
            $scope.performSearch();
        }

    });


})(angular);