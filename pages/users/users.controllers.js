(function (angular) {

    var module = angular.module('usersModule', []);

    module.controller("listUsersController", function ($scope, $timeout, DataAccess, ngToast) {

        $scope.loading = true;
        $scope.totalItems = 0;
        $scope.itemsPerPage = 5;

        $scope.getDetails = function () {
            $scope.users = $scope.results;
            $scope.totalItems = $scope.users.length;
            $scope.loading = false
        };

        DataAccess.read("users").then(function (response) {
            $scope.results = response;
        });

        $timeout(function () {
            $scope.getDetails();
        }, 1000);

    });

    module.controller('addUserController', function ($scope, $state, $timeout, DataAccess, ngToast) {
        $scope.usernameValid = false;
        $scope.passwordValid = false;
        $scope.usernameResponse = '';
        $scope.userType = 1;
        $scope.title = 'Add user';

        $scope.setUserType = function (userType) {
            $scope.userType = userType;
        }

        // $scope.checkPassword = function (user) {
        //     console.log('P:' + user.password + ' CP:' + user.cpassword);
        //     $scope.passwordValid = false;
        //     $scope.passwordResponse = '';
        //     if (user.password != user.cpassword) {
        //         $scope.passwordResponse = 'passwords do not match';
        //     } else {
        //         $scope.passwordValid = true;
        //         $scope.passwordResponse = 'perfect';
        //     }
        // }

        // $scope.checkUsername = function (user) {
        //     $scope.checkingUsername = true;
        //     $scope.usernameResponse = '';
        //     $scope.usernameValid = false;
        //     DataAccess.checkUsername({ username: user.username })
        //         .then(function (result) {
        //             $scope.checkingUsername = false;
        //             if (result.status) {
        //                 $scope.usernameResponse = result.message;
        //             } else {
        //                 if ($scope.username != "") {
        //                     $scope.usernameValid = true;
        //                     $scope.usernameResponse = user.username + ' is perfect';
        //                 }
        //             }
        //         });
        // }
        $scope.action = 'add';

        $scope.save = function (user) {

            var details = {
                firstName: user.firstName,
                lastName: user.lastName,
                username: user.username,
                password: user.password,
                userType: $scope.userType,
            };

            DataAccess.create("users", details).then(function (result) {
                if (result) {
                    ngToast.create({ className: 'success', content: '<i class="fa fa-check-circle"></i> Saved successful' });
                    $state.go('users');
                } else {
                    ngToast.create({ className: 'danger', content: '<i class="fa fa-warning"></i> Failed!' });
                }
            });
        }
    });

    module.controller('editUserController', function ($scope, $state, $stateParams, $timeout, DataAccess, ngToast) {
        $scope.usernameValid = false;
        $scope.passwordValid = false;
        $scope.loading = true;
        $scope.usernameResponse = '';
        $scope.userType = 1;
        $scope.action = 'edit';

        var id = $stateParams.userId;
        DataAccess.get("users", id).then(function (response) {
            $scope.results = response[0];
        });

        $scope.getDetails = function () {
            $scope.loading = false;
            $scope.user = $scope.results;
            $scope.title = 'Edit user: ' + $scope.user.firstName;
        };

        $timeout(function () {
            $scope.getDetails();
        }, 1000);

        $scope.setUserType = function (userType) {
            $scope.userType = userType;
        }

        $scope.checkUsername = function () {
            $scope.checkingUsername = true;
            $scope.usernameResponse = '';
            $scope.usernameValid = false;
            DataAccess.checkUsername({ username: $scope.username })
                .then(function (result) {
                    $scope.checkingUsername = false;
                    if (result.status) {
                        $scope.usernameResponse = result.message;
                    } else {
                        if ($scope.username != "") {
                            $scope.usernameValid = true;
                            $scope.usernameResponse = $scope.username + ' is perfect';
                        }
                    }
                });
        }

        $scope.save = function (user) {
            var details = {
                firstName: user.firstName,
                lastName: user.lastName,
                username: user.username,
                password: user.password,
                userType: $scope.userType,
                entityStatus: user.entityStatus,
            };
           
            DataAccess.update("users", $stateParams.userId, details).then(function (result) {
                if (result) {
                    ngToast.create({ className: 'success', content: '<i class="fa fa-check-circle"></i> Saved successful' });
                    $state.go('users');
                } else {
                    ngToast.create({ className: 'danger', content: '<i class="fa fa-warning"></i> Failed' });
                }
            });
        }
    });

})(angular);