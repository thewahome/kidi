const { app, BrowserWindow, ipcMain } = require("electron")

app.on("ready", () => {
    //let server = require('./server/knex.js');
    let mainWindow = new BrowserWindow({ height: 800, width: 1300, show: true, icon: `file://${__dirname}/assets/images/favicon.png` })
    mainWindow.loadURL(`file://${__dirname}/main.html`)
    mainWindow.once("ready-to-show", () => { mainWindow.show() })

    ipcMain.on("mainWindowLoaded", function () {
        // let result = server.getPeople()
        // result.then(function(rows) {
        //     mainWindow.webContents.send("resultSent", rows);
        // })
    });
});



app.on("window-all-closed", () => { app.quit() })