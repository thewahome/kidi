var knex = require("knex")({
    client: "sqlite3",
    connection: {
        filename: "./database.sqlite"
    }
});

this.getPeople = function() {
    try {
        let result = knex.select("*").from("User");
        return result
    } catch (error) {
        console.log('model.getPeople', error.message)
    }
}