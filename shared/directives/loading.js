(function(angular) {
    churchApp.directive('loadingSpinner', function() {
        return {
            template: '<div class="text-center text-primary"><i class="fa fa-spinner xxlarge-icon fa-spin"></i><p class=" large-text">Loading</p></div>'
        };
    });
})(angular);