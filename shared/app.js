var BASE_URL = "http://localhost:81/church/";
var churchApp = angular.module("churchApp", ['routingModule', 'ngResource', 'ngStorage', 'Database',
    'loginModule', 'churchServices', 'dataModule', 'classesModule', 'studentsModule', 'dashboardModule', 'angularUtils.directives.dirPagination', 'ngCsvImport', 'chart.js',
    , 'feesModule', 'usersModule', 'searchModule', 'ngToast', 'ui.bootstrap', 'ui.select', 'ngSanitize'
]);

churchApp.config([
    'ngToastProvider',
    function (ngToastProvider) {
        ngToastProvider.configure({
            animation: 'slide',
            horizontalPosition: 'right',
            verticalPosition: 'bottom',
            maxNumber: 1,
            timeout: 4000
        });
    }
]);
churchApp.filter('dateToHuman', function ($filter) {
    return function (input) {
        if (input == null) { return ""; }
        var a = new Date(input * 1000);
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var year = a.getFullYear();
        var month = months[a.getMonth()];
        var date = a.getDate();
        var time = date + ' ' + month;
        return time;
    }
});

churchApp.filter('customSplitString', function () {
    return function (input) {
        var arr = input.split(',');
        return arr;
    };
});



churchApp.controller('mainController', function ($rootScope, $scope, $state, $localStorage, $sessionStorage) {
    this.stateName = function () {
        return $state.current.name;
    };

    $scope.displayMenu = true;
    this.displayMenu = function () {
        if ($state.current.name == 'fee-receipt' ) {
            return false;
        } 
        return $scope.displayMenu;
    }
    this.loggedInUser = function () {
        if ($localStorage.user != null) {
            $fullname = $localStorage.user.firstName + ' ' + $localStorage.user.lastName;
            $scope.displayMenu = true;
            return $fullname;
        } else {
            $scope.displayMenu = false;
            $state.go("login");
        }
    };

    $scope.search = function (searchText) {
        $state.go('search', { "searchText": searchText });
    };

    $scope.$storage = $rootScope.$storage = $localStorage;
    $scope.$sessionStorage = $rootScope.$sessionStorage = $sessionStorage;
    $scope.canEdit = false;

    if ($localStorage.user != null && $localStorage.user.userType == 0)
        $scope.canEdit = true;

    $scope.logout = function () {
        $localStorage.user = null;
        $state.go("login");
    };

});