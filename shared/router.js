(function (angular) {

    var module = angular.module('routingModule', ['ui.router']);

    var redirectToLogin = function () {
        sessionStorage.nextPage = document.URL;
        var baseLocation = protocolUrl();
        window.location = baseLocation + '/#/login';
    };

    var protocolUrl = function () {
        pathArray = location.href.split('/');
        protocol = pathArray[0];
        host = pathArray[2] + '/' + pathArray[3];
        url = protocol + '//' + host;
        return url;
    };

    module.config(function ($stateProvider, $urlRouterProvider) {


        $stateProvider
            .state('login', {
                url: '/login',
                controller: 'loginController as LC',
                templateUrl: 'pages/login/login.html'
            })
            .state('logout', {
                url: '/logout',
                templateUrl: 'pages/login/logout.html'
            })

            .state('dashboard', {
                url: '/dashboard',
                templateUrl: 'pages/dashboard/dashboard.html',
                controller: 'dashboardController',
            })

            .state('search', {
                url: '/search/:searchText',
                templateUrl: 'pages/search/search.html',
                controller: 'searchController',
            })
            .state('classes', {
                url: '/classes/',
                templateUrl: 'pages/classes/list.html',
                controller: 'listClassesController as LCC'
            })
            .state('classadd', {
                url: '/classes/add/',
                templateUrl: 'pages/classes/edit.html',
                controller: 'addClassController',
            })
            .state('classedit', {
                url: '/classes/edit/:id/',
                templateUrl: 'pages/classes/edit.html',
                controller: 'editClassController',
            })
            .state('streams', {
                url: '/streams',
                templateUrl: 'pages/streams/list.html',
                controller: 'listStreamsController'
            })
            .state('streamadd', {
                url: '/streams/add/',
                templateUrl: 'pages/streams/edit.html',
                controller: 'addStreamController',
            })
            .state('streamedit', {
                url: '/streams/edit/:id/',
                templateUrl: 'pages/streams/edit.html',
                controller: 'editStreamController',
            })

            .state('students', {
                url: '/students',
                templateUrl: 'pages/students/options.html'
            })

            .state('studentlist', {
                url: '/students/list/',
                templateUrl: 'pages/students/list.html',
                controller: 'listStudentsController',
            })

            .state('studentadd', {
                url: '/students/add/',
                templateUrl: 'pages/students/edit.html',
                controller: 'addStudentController',
            })

            .state('studentedit', {
                url: '/students/edit/:id/',
                templateUrl: 'pages/students/edit.html',
                controller: 'editStudentController',
            })

            .state('studentupload', {
                url: '/students/upload/',
                templateUrl: 'pages/students/upload.html',
                controller: 'uploadStudentController',
            })

            .state('studentdetail', {
                url: '/students/details/:id',
                templateUrl: 'pages/students/detail.html',
                controller: 'studentDetailController',
            })

            .state('fee-setup', {
                url: '/fees/setup/',
                templateUrl: 'pages/fees/setup.html',
                controller: 'setupFeesController',
            })

            .state('fee-payment', {
                url: '/fees/payment/',
                templateUrl: 'pages/fees/payment.html',
                controller: 'payFeesController',
            })

            .state('fee-reports', {
                url: '/fees/reports/',
                templateUrl: 'pages/fees/reports.html',
                controller: 'feeReportsController',
            })

            .state('fee-receipt', {
                url: '/fees/receipt/:id',
                templateUrl: 'pages/fees/receipt.html',
                controller: 'feeReceiptController',
            })

           
            .state('users', {
                url: '/users',
                controller: 'listUsersController as LUC',
                templateUrl: 'pages/users/list.html'
            })

            .state('useradd', {
                url: '/users/add/',
                templateUrl: 'pages/users/edit.html',
                controller: 'addUserController',
            })
            .state('useredit', {
                url: '/users/edit/:id/',
                templateUrl: 'pages/users/edit.html',
                controller: 'editUserController',
            })
 


            ;

        $urlRouterProvider.otherwise('dashboard');

    }).run(function ($rootScope, $location, $state) {

        $rootScope.nextPage = 'dashboard';

        $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {


        });

        $rootScope.$on('$stateChangePermissionDenied', function (e, from, to) {
            if (from.data.permissions.redirectTo)
                $rootScope.nextPage = from.name;
        });

    });

})(angular);