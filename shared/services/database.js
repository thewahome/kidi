(function (angular) {

    var module = angular.module('Database', []);

    var knex = require("knex")({
        client: "sqlite3",
        connection: {
            filename: "./database.sqlite"
        }
    });

    module.service('DataAccess', function ($q) {
        return {
            read: read,
            get: get,
            create: create,
            update: update,
            pages: pages,
            search: search,
            checkAmount: checkAmount,
            checkPayments: checkPayments,
            history: history,
            findUser: findUser,
            // getByName: getCustomerByName,
            // destroy: deleteCustomer,
            // update: updateCustomer
        };

        function read(table) {
            try {
                var deferred = $q.defer();
                let result = knex.select().table(table);
                result.then(function (rows) {
                    deferred.resolve(rows);
                });
                return deferred.promise;
            } catch (error) {
                console.log(table + 'read failed: ', error.message)
            }
        }

        function pages(table, filters) {
            try {
                var deferred = $q.defer();
                var query = {};
                if (filters.class) {
                    query.classId = filters.class ;
                }
                if (filters.stream && filters.class) {
                    query = { classId: filters.class, streamId: filters.stream }
                }
                if (filters.term && filters.year) {
                   query = { term: filters.term, year: filters.year }
                }

                let result = knex.select().table(table).where(query);
                result.then(function (rows) {
                    deferred.resolve(rows);
                });
                return deferred.promise;
            } catch (error) {
                console.log(table + 'read failed: ', error.message)
            }
        }
        function get(table, id) {
            try {
                var deferred = $q.defer();
                let result = knex(table).where('id', id);
                result.then(function (rows) {
                    deferred.resolve(rows);
                });
                return deferred.promise;
            } catch (error) {
                console.log(table + 'get failed: ', error.message)
            }
        }
        function create(table, data) {
            try {
                var deferred = $q.defer();
                data.dateCreated = new Date().toLocaleString();
                data.dateLastModified = new Date().toLocaleString();
                let result = knex(table).returning('id').insert(data);
                result.then(function (id) {
                    deferred.resolve(id);
                });
                return deferred.promise;
            } catch (error) {
                console.log(table + ' create failed: ', error.message);
                return error.message
            }
        }
        function update(table, id, data) {
            try {
                var deferred = $q.defer();
                data.dateLastModified = new Date().toLocaleString();
                let result = knex(table).where('id', id).update(data);
                result.then(function (id) {
                    deferred.resolve(id);
                });
                return deferred.promise;
            } catch (error) {
                console.log(table + 'update failed: ', error.message)
            }
        }
        function search(table, searchTerm) {
            try {
                var deferred = $q.defer();
                let result = knex(table)
                    .where('studentId', searchTerm)
                    .orWhere('otherNames', 'like', "%" + searchTerm + "%")
                    .orWhere('surname', 'like', "%" + searchTerm + "%")
                    .orWhere('phone1', 'like', "%" + searchTerm + "%")
                    .orWhere('phone2', 'like', "%" + searchTerm + "%")
                    ;
                result.then(function (rows) {
                    deferred.resolve(rows);
                });
                return deferred.promise;
            } catch (error) {
                console.log(table + 'get failed: ', error.message)
            }
        }

        function checkAmount(table, term, year, classId) {
            try {
                var deferred = $q.defer();
                var query = {
                    term: term,
                    year: year,
                }

                if (classId) {
                    query = {
                        term: term,
                        year: year,
                        classId: classId,
                    }
                }
                let result = knex(table)
                    .where(query);
                result.then(function (rows) {
                    deferred.resolve(rows);
                });
                return deferred.promise;
            } catch (error) {
                console.log(table + 'get failed: ', error.message)
            }
        }

        function findUser(details) {
            try {
                debugger
                var deferred = $q.defer();
                let result = knex("users").where(details);
                result.then(function (rows) {
                    deferred.resolve(rows);
                });
                return deferred.promise;
            } catch (error) {
                console.log(table + 'get failed: ', error.message)
            }
        }

        function checkPayments(table, term, year, studentId) {
            try {
                var deferred = $q.defer();
                var query = {}
                if (term) {
                    query.term = term;
                }
                if (term && year) {
                    query = { term: term, year: year }
                }
                if (studentId) {
                    query.studentId = studentId;
                }


                let result = knex(table)
                    .where(query);
                result.then(function (rows) {
                    deferred.resolve(rows);
                });
                return deferred.promise;
            } catch (error) {
                console.log(table + 'get failed: ', error.message)
            }
        }


    });

})(angular);