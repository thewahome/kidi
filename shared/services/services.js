(function(angular) {

    var module = angular.module('churchServices', []);

    module.factory("Login", function($resource) {
        return $resource(BASE_URL + 'login', {}, {
            findUser: {
                url: BASE_URL + 'login',
                method: "POST"
            },
            resendPassword: {
                url: BASE_URL + 'resendpassword',
                method: "POST"
            }
        });
    });

    module.factory("Dashboard", function($resource) {
        return $resource(BASE_URL + 'dashboard', {}, {
            genders: {
                url: BASE_URL + 'dashboard/genders',
                method: "GET"
            },
            counters: {
                url: BASE_URL + 'dashboard/counters',
                method: "GET"
            },
            maritalstatuses: {
                url: BASE_URL + 'dashboard/maritalstatuses',
                method: "GET"
            },
            membershipstatuses: {
                url: BASE_URL + 'dashboard/membershipstatuses',
                method: "GET"
            },
            years: {
                url: BASE_URL + 'dashboard/years',
                method: 'POSt',
            },
            districts: {
                url: BASE_URL + 'dashboard/districts',
                method: 'GET',
            },
        });
    });

    module.factory("Groups", function($resource) {
        return $resource(BASE_URL + 'groups', {}, {
            create: {
                url: BASE_URL + 'groups/create',
                method: "POST"
            },
            status: {
                url: BASE_URL + 'groups/status',
                method: "POST"
            },
            read: {
                url: BASE_URL + 'groups/read/id/:id',
                method: "GET",
                params: {
                    id: '@id'
                }
            },
            fetch: {
                url: BASE_URL + 'groups/fetch/groupType/:groupType',
                method: "GET",
                params: {
                    groupType: '@groupType'
                }
            },
            pages: {
                url: BASE_URL + 'groups/pages/groupType/:groupType/pageNumber/:pageNumber/itemsPerPage/:itemsPerPage',
                method: 'GET',
                params: {
                    groupType: '@groupType',
                    itemsPerPage: '@itemsPerPage',
                    pageNumber: '@pageNumber'
                }
            },
            members: {
                url: BASE_URL + 'groupmembers/pages/id/:id/pageNumber/:pageNumber/itemsPerPage/:itemsPerPage',
                method: 'GET',
                params: {
                    id: '@id',
                    itemsPerPage: '@itemsPerPage',
                    pageNumber: '@pageNumber'
                }
            },

        });
    });

    module.factory("Members", function($resource) {
        return $resource(BASE_URL + 'members', {}, {
            create: {
                url: BASE_URL + 'members/create',
                method: "POST"
            },
            update: {
                url: BASE_URL + 'members/update',
                method: "POST"
            },
            read: {
                url: BASE_URL + 'members/read',
                method: "GET"
            },
            details: {
                url: BASE_URL + 'members/details/id/:id',
                method: 'GET',
                params: {
                    id: '@id',
                }
            },
            pages: {
                url: BASE_URL + 'members/pages/pageNumber/:pageNumber/itemsPerPage/:itemsPerPage',
                method: 'GET',
                params: {
                    itemsPerPage: '@itemsPerPage',
                    pageNumber: '@pageNumber'
                }
            }

        });
    });

    module.factory("GroupMembers", function($resource) {
        return $resource(BASE_URL + 'groupmembers', {}, {
            create: {
                url: BASE_URL + 'groupmembers/create',
                method: "POST"
            },
            status: {
                url: BASE_URL + 'groupmembers/status',
                method: "POST"
            },
            read: {
                url: BASE_URL + 'groupmembers/read/id/:id',
                method: "GET",
                params: {
                    id: '@id'
                }
            },
        });
    });

    module.factory("Roles", function($resource) {
        return $resource(BASE_URL + 'roles', {}, {
            read: {
                url: BASE_URL + 'roles/read',
                method: "GET"
            },
            create: {
                url: BASE_URL + 'roles/create',
                method: "POST"
            },
        });
    });

    module.factory("Professions", function($resource) {
        return $resource(BASE_URL + 'professions', {}, {
            read: {
                url: BASE_URL + 'professions/read',
                method: "GET"
            },
            create: {
                url: BASE_URL + 'professions/create',
                method: "POST"
            },
        });
    });

    module.factory("Ministries", function($resource) {
        return $resource(BASE_URL + 'ministries', {}, {
            read: {
                url: BASE_URL + 'ministries/read',
                method: "GET"
            },
            create: {
                url: BASE_URL + 'ministries/create',
                method: "POST"
            },
            update: {
                url: BASE_URL + 'ministries/update',
                method: "POST"
            },

        });
    });

    module.factory("Search", function($resource) {
        return $resource(BASE_URL + 'search', {}, {
            search: {
                url: BASE_URL + 'search/index',
                method: 'POST',
            },
            reports: {
                url: BASE_URL + 'search/reports',
                method: 'POST',
            }
        });
    });

    module.factory("Reports", function($resource) {
        return $resource(BASE_URL + 'search', {}, {
            search: {
                url: BASE_URL + 'search/index',
                method: 'POST',
            }
        });
    });

    module.factory("GroupMemberRoles", function($resource) {
        return $resource(BASE_URL + 'groupmemberroles', {}, {
            create: {
                url: BASE_URL + 'groupmemberroles/create',
                method: "POST"
            },
            status: {
                url: BASE_URL + 'groupmemberroles/status',
                method: "POST"
            },
            read: {
                url: BASE_URL + 'groupmemberroles/read/id/:id',
                method: "GET",
                params: {
                    id: '@id'
                }
            },
        });
    });

    module.factory("Users", function($resource) {
        return $resource(BASE_URL + 'users', {}, {
            read: {
                url: BASE_URL + 'users/read',
                method: "GET"
            },
            create: {
                url: BASE_URL + 'users/create',
                method: "POST"
            },
            update: {
                url: BASE_URL + 'users/update',
                method: "POST"
            },
            checkUsername: {
                url: BASE_URL + 'users/checkUsername',
                method: "POST"
            },
        });
    });

    module.factory("Messages", function($resource) {
        return $resource(BASE_URL + 'messages', {}, {
            create: {
                url: BASE_URL + 'messages/create',
                method: "POST"
            },
            pages: {
                url: BASE_URL + 'messages/pages/pageNumber/:pageNumber/itemsPerPage/:itemsPerPage',
                method: 'GET',
                params: {
                    itemsPerPage: '@itemsPerPage',
                    pageNumber: '@pageNumber'
                }
            }
        });
    });

    module.factory('Storage', function($localStorage) {
        return {
            currentUser: function() {
                var user = $localStorage.user;
                return $localStorage.user;
            },
            canEdit: function() {
                var user = $localStorage.user;
                if (user.userType != 0) return false;
                return true;
            },
        }
    });

})(angular);